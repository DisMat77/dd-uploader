$(document).ready(function(){
    $('#drop-area').on('dragover', function(e){
        e.preventDefault();
        e.stopPropagation();
    });
    $('#drop-area').on('dragenter', function(e){
        e.preventDefault();
        e.stopPropagation();
    });
    $('#drop-area').on('drop', function(e){
        e.preventDefault();
        e.stopPropagation();
    });

        // let col_item = document.querySelectorAll('gallery__item');
        // col_item = 0;

    $('#inputFile').on('change', function(){
        let reader = new FileReader();
        reader.readAsDataURL(this.files[0]);
        let galleryItems = $('.gallery__item');

        reader.onload = function(event) {
            if (event.lengthComputable) {
                let progressBox = document.querySelector('.progressBox');
                let progress = (event.loaded/event.total)*100;
                progressBox.innerText = progress + '%';
            }
        };

        reader.onloadend = function() {
            let srcImg = reader.result;
            let newImg = document.createElement('IMG');
            let newItem = document.createElement('LI');
            let gallery = document.querySelector('.gallery__list');
            newImg.src = srcImg;
            newItem.append(newImg);
            gallery.prepend(newItem);
            gallery.style.cssText = 'list-style: none;';
            newItem.classList.add('gallery__item');
            };



        // col_item++;
        // console.log(col_item);
    });



    $('#drop-area').bind('drop', function(event){
        let file = event.originalEvent.dataTransfer.files[0];

        let reader = new FileReader();
        let newImg = document.createElement('IMG');
        let newItem = document.createElement('LI');
        let gallery = document.querySelector('.gallery__list');

        reader.onload = function(e2) {
            let newImg = document.createElement('img');
            newImg.src= e2.target.result;
            // document.body.appendChild(newImg);
            newItem.append(newImg);
            gallery.prepend(newItem);
            gallery.style.cssText = 'list-style: none;';
            newItem.classList.add('gallery__item');

        };

        reader.readAsDataURL(file);
        // col_item++;
        // console.log(col_item);
    });

});